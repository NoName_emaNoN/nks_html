$(function () {
    $('.b-range-slider').each(function () {
        var $slider = $(this).find('.b-range-slider__slider');
        var $inputFrom = $(this).find('.b-range-slider__input_from');
        var $inputTo = $(this).find('.b-range-slider__input_to');
        var $labelFrom = $(this).find('.b-range-slider__value_from');
        var $labelTo = $(this).find('.b-range-slider__value_to');
        var ratio = parseInt($slider.data('ratio')) ? parseInt($slider.data('ratio')) : 1;

        $slider.slider({
            range: true,
            min: $slider.data('min'),
            max: $slider.data('max'),
            step: $slider.data('step') ? $slider.data('step') : 1,
            values: [$inputFrom.val(), $inputTo.val()],
            slide: function (event, ui) {
                $labelFrom.text(ui.values[0]);
                $inputFrom.val(ui.values[0] * ratio);
                $labelTo.text(ui.values[1]);
                $inputTo.val(ui.values[1] * ratio);
            }
        });

        $labelFrom.text($slider.slider("values", 0));
        $inputFrom.val($slider.slider("values", 0) * ratio);
        $labelTo.text($slider.slider("values", 1));
        $inputTo.val($slider.slider("values", 1) * ratio);
    });
});
