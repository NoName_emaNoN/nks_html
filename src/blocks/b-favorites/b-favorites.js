$(function () {
    if ($('.b-favorites__list .b-catalog__list').length) {
        function recalcFavoritesWidth() {
            var $list = $('.b-favorites__list .b-catalog__list');
            var $items = $list.children('.b-catalog__item');

            if ($(window).outerWidth() <= 767) {
                $list.css('width', 'auto');
            } else {
                if ($items.length) {
                    $list.width(($items.width() + 20) * $items.length);
                }
            }
        }

        window.onresize = recalcFavoritesWidth;

        recalcFavoritesWidth();

        jQuery('.b-favorites__list').scrollbar({
            "autoScrollSize": false,
            "scrollx": $('.external-scroll_x'),
            //"scrolly": $('.external-scroll_y')
        });
    }
});
