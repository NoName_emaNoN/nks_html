$(function () {
    $(document).on('click', '.b-catalog__expand', function (e) {
        e.preventDefault();

        $(this).closest('.b-catalog__cover').toggleClass('b-catalog__cover_expanded');
    });
});
