$(function () {
    $('.b-card__image_zoom').each(function () {
        var url = $(this).data('url');

        $(this).zoom({url: url});
    });

    $('a.b-card__image').on('click', function () {
        if ($(window).width() < 768) {
            var url = $(this).data('url');

            window.open(url);
        }
    });

    $("a.b-card__image").fancybox({
        type: 'inline',
        fitToView: false,
        margin: [20, 0, 20, 0],
        padding: 0,
        beforeLoad: function () {
            if ($(window).width() < 768) {
                return false;
            }
        }
    });
});
