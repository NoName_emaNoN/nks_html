$(function () {
    $(document).on('click', '.b-navbar .navbar-toggle.collapsed', function () {
        $('body').addClass('modal-open');
        $($(this).data('target')).addClass('in');
    });
    $(document).on('click', '.b-navbar .navbar-toggle:not(.collapsed)', function () {
        $('body').removeClass('modal-open');
        $($(this).data('target')).removeClass('in');
    });
    $(".fancybox-inline").fancybox({type: 'inline', fitToView: false, margin: [20, 0, 20, 0], padding: 0});
});
$(".fancybox-remote").fancybox({type: 'ajax', fitToView: false, margin: [20, 0, 20, 0], padding: 0, autoHeight: true, autoWidth: true});
